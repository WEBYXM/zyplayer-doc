package com.zyplayer.doc.data.service.manage;

import com.zyplayer.doc.data.repository.manage.entity.WikiSpace;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-03-13
 */
public interface WikiSpaceService extends IService<WikiSpace> {
	
	List<Long> getViewAuthSpaceIds();
	
}
